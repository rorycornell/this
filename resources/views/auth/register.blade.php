@extends('layouts.app')

@section('content')
<div class="main-content-wrapper">
                <div class="panel-heading">Register</div>
                    <form class="tr-container" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <h2>Register as a Music Therapist</h2>
                        <fieldset>
                          <h3>Account Information</h3>

                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <label for="email">E-Mail Address</label><input id="email" type="email" placeholder="Enter Email Address Here..." class="form-control" name="email" value="{{ old('email') }}" required>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>


                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Password</label><input id="password" type="password" class="form-control inputpass" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                          <div class="form-group">
                              <label for="password-confirm" class="o-border-bottom">Confirm Password</label><input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                          </div>
                        </fieldset>
                        <fieldset>
                          <h3>Personal Information</h3>
                          <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                          <label>First Name</label><input id="firstname" type="text" placeholder="Enter First Name Here.." class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>
                          @if ($errors->has('firstname'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('firstname') }}</strong>
                              </span>
                          @endif
                          </div>
                          <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                          <label>Last Name</label><input required="" type="text" placeholder="Enter Last Name Here.." class="form-control" name="lastname" id="lastname" value="{{ old('lastname') }}" required autofocus>
                          @if ($errors->has('lastname'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('lastname') }}</strong>
                              </span>
                          @endif
                        </div>
                          <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                          <label>Address</label><input type="text" placeholder="Enter Street Address Here.." class="form-control" name="street" id=street value="{{ old('street') }}" required autofocus>
                          @if ($errors->has('street'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('street') }}</strong>
                              </span>
                          @endif
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <label>City</label><input type="text" placeholder="Enter City Here.." class="form-control" name="city" id=city value="{{ old('city') }}" required autofocus>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                        </div>
                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                        <label>State</label><input type="text" placeholder="Enter State Here.." class="form-control" name="state" id=state value="{{ old('state') }}" required autofocus>
                        @if ($errors->has('state'))
                            <span class="help-block">
                                <strong>{{ $errors->first('state') }}</strong>
                            </span>
                        @endif
                        </div>

                        <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                        <label>Zip</label><input type="text" placeholder="Enter Zip Code Here.." class="form-control" name="zip" id=zip value="{{ old('zip') }}" required autofocus>
                        @if ($errors->has('zip'))
                            <span class="help-block">
                                <strong>{{ $errors->first('zip') }}</strong>
                            </span>
                        @endif
                        </div>
                        </fieldset>
                        <fieldset>
                          <h3>Practice Information</h3>

                          <div class="form-group{{ $errors->has('affiliation') ? ' has-error' : '' }}">
                          <label>Affiliation</label><input type="text" placeholder="Enter Affiliation Here.." class="form-control" name="affiliation" id=affiliation value="{{ old('affiliation') }}" required autofocus>
                          @if ($errors->has('affiliation'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('affiliation') }}</strong>
                              </span>
                          @endif
                          </div>

                          <div class="form-group{{ $errors->has('certification') ? ' has-error' : '' }}">
                          <label>Certification</label><input type="text" placeholder="Enter Certification Here.." class="form-control" name="certification" id=certification value="{{ old('certification') }}" required autofocus>
                          @if ($errors->has('certification'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('certification') }}</strong>
                              </span>
                          @endif
                          </div>

                          <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                          <label>Phone</label><input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="phone" id=phone value="{{ old('phone') }}" required autofocus>
                          @if ($errors->has('phone'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                          @endif
                          </div>

                          <div class="form-group{{ $errors->has('distance_served') ? ' has-error' : '' }}">
                          <label>Distance Served</label><input type="text" placeholder="In miles..." class="form-control" name="distance_served" id=distance_served value="{{ old('distance_served') }}" required autofocus>
                          @if ($errors->has('distance_served'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('distance_served') }}</strong>
                              </span>
                          @endif
                          </div>
                          <label class="no-border-bottom">Private Sessions</label><input type="text" id="private_sessions" data-toggle="toggle" data-on="1" data-off="0" name="private_sessions">
                        </fieldset>









                        <div class="form-group">

                            			<button class="tr-btn" type="submit">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
