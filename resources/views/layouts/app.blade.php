<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- TITLE -->
		<title>Point Motion | Find A Music Therapist</title>

		<!-- META TAGS -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="Find a music therapist in your area.">
		<meta name="keywords" content="Find, Music, Therapist, Point, Motion, Therapy, Search, Near, Me">
	    <meta name="author" content="Point Motion">
		<meta name="copyright" content="Point Motion 2017"/>

		<!-- SCRIPTS FOR LOAD -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	    <!-- ANALYTICS -->

	    <!-- FONTS -->
	    <link href="" rel="stylesheet">

		<!-- STYLES -->



		 <link href="{{ asset('/css/styles.css') }}" rel="stylesheet">

		<!-- FAVICONS -->
		<link rel="apple-touch-icon" sizes="180x180" href="ico/">
		<link rel="icon" type="image/png" sizes="32x32" href="ico/">
		<link rel="icon" type="image/png" sizes="194x194" href="ico/">
		<link rel="icon" type="image/png" sizes="192x192" href="ico/">
		<link rel="icon" type="image/png" sizes="16x16" href="ico/">
		<link rel="manifest" href="ico/">
		<link rel="mask-icon" href="ico/" color="#000000">
		<link rel="shortcut icon" href="ico/">
		<meta name="apple-mobile-web-app-title" content="ico/">
		<meta name="application-name" content="ico/">
		<meta name="msapplication-TileColor" content="ico/">
		<meta name="msapplication-TileImage" content="ico/">
		<meta name="msapplication-config" content="ico/">
		<meta name="theme-color" content="#000000">

		<!-- OPEN GRAPH -->
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://www.findamusictherapist.com"/>
		<meta property="og:description" content=""/>
		<meta property="og:title" content="Point Motion | Find A Music Therapist"/>
		<meta property="og:image" content="ico/"/>

		<!-- TWITTER -->
		<meta name="twitter:card" content=""/> <!-- Card Types: “summary”, “summary_large_image”, “app”, or “player” -->
		<meta name="twitter:site" content="@"/> <!-- Twitter Account Associated with Site -->
		<meta name="twitter:creator" content="@"/> <!-- Twitter Account Associated with Creator -->
	</head>

	<body>
		<!-- GLOBAL HEADER -->
		<header class="global-header" title="">
			<div class="gh-logo">
				<a class="gh-toggle" href="javascript:;" title="Show Menu"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200"><polygon points="200 72.63 127.37 72.63 127.37 0 72.63 0 72.63 72.63 0 72.63 0 127.37 72.63 127.37 72.63 200 127.37 200 127.37 127.37 200 127.37 200 72.63"/></polygon></svg></a><!--<img src="img/logo.png" alt="Point Motion | Find A Music Therapist">--><a href="http://www.matthewlewicki.com/work/pointmotion" title="Point Motion | Find A Music Therapist Home"><h1>Find A Music Therapist</h1></a>
			</div>
			<!-- GLOBAL HEADER NAV -->
			<nav class="gh-nav">
				<ul>
					<li class="gh-nav-search"><a href="{{ url('/') }}">Search Therapists</a></li>
          <li class="gh-nav-about"><a href="{{ url('/about') }}">About</a></li>
          @if (Auth::guest())
          <li class="gh-nav-get-listed"><a href="{{ url('/register') }}">Get Listed</a></li>
          <li class="gh-nav-login"><a class="gh-nav-login-link" href="javascript:;">Login</a></li>
            <div class="gh-nav-login-popover">
      				<form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
      					<a class="gh-nav-login-popover-dismiss" href="javascript:;" title="Close Login Pop Up"><svg viewbox="0 0 40 40"><path d="M 10,10 L 30,30 M 30,10 L 10,30"/></svg></a>
      					<h2>Login as a Music Therapist</h2>
      					<!--<input id="email" type="email" title="Enter Your Email Address" placeholder="Email Address"/>-->
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
      					<!--<input id="password" type="password" title="Enter Your Password" placeholder="Password"/>-->
                <input id="password" type="password" class="form-control" name="password" required>
                <input type="submit" title="Submit" value="Sign In">
      				</form>
      			</div>

          @else
          <li class="gh-nav-get-listed">{{ Auth::user()->name }}</li>
          <li class="gh-nav-login"><a href="{{ route('logout') }}"
              onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
              Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>

              @endif
          </ul>

			</nav>
		</header>
		<script>
			$(document).ready(function(){
				$('.gh-toggle').click(function(){
					$(this).closest('.global-header').find('.gh-nav').toggleClass('gh-nav-open');
					$(this).toggleClass('gh-logo-open');
					});
				$('.gh-nav-login-link').click(function(){
					$(this).closest('.global-header').find('.gh-nav-login-popover').toggleClass('gh-nav-login-popover-open');
					});
				$('.gh-nav-login-popover-dismiss').click(function(){
					$(this).closest('.global-header').find('.gh-nav-login-popover').toggleClass('gh-nav-login-popover-open');
					});
			});
		</script>
		<body>

			@yield('content')


		</body>



		<!-- PAGE CONTENT -->
		<div class="main-content-wrapper">
		</div>
		<!-- PAGE FOOTER -->
		<footer>
			<p>Copyright 2017 Point Motion</a>
		</footer>
	</body>
</html>
