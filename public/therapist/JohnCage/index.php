<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- TITLE -->
		<title>Point Motion | Find A Music Therapist | John Cage</title>

		<!-- META TAGS -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="Find a music therapist in your area.">
		<meta name="keywords" content="Find, Music, Therapist, Point, Motion, Therapy, Search, Near, Me">
	    <meta name="author" content="Point Motion">
		<meta name="copyright" content="Point Motion 2017"/>

		<!-- SCRIPTS FOR LOAD -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	    <!-- ANALYTICS -->

	    <!-- FONTS -->
	    <link href="" rel="stylesheet">

		<!-- STYLES -->
		<link rel="stylesheet" href="../../styles.css" type="text/css">
		
		<!-- FAVICONS -->
		<link rel="apple-touch-icon" sizes="180x180" href="../../ico/">
		<link rel="icon" type="image/png" sizes="32x32" href="../../ico/">
		<link rel="icon" type="image/png" sizes="194x194" href="../../ico/">
		<link rel="icon" type="image/png" sizes="192x192" href="../../ico/">
		<link rel="icon" type="image/png" sizes="16x16" href="../../ico/">
		<link rel="manifest" href="../../ico/">
		<link rel="mask-icon" href="../../ico/" color="#000000">
		<link rel="shortcut icon" href="../../ico/">
		<meta name="apple-mobile-web-app-title" content="../../ico/">
		<meta name="application-name" content="../../ico/">
		<meta name="msapplication-TileColor" content="../../ico/">
		<meta name="msapplication-TileImage" content="../../ico/">
		<meta name="msapplication-config" content="../../ico/">
		<meta name="theme-color" content="#000000">

		<!-- OPEN GRAPH -->
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://www.findamusictherapist.com"/>
		<meta property="og:description" content=""/>
		<meta property="og:title" content="Point Motion | Find A Music Therapist | John Cage"/>
		<meta property="og:image" content="ico/"/>

		<!-- TWITTER -->
		<meta name="twitter:card" content=""/> <!-- Card Types: “summary”, “summary_large_image”, “app”, or “player” -->
		<meta name="twitter:site" content="@"/> <!-- Twitter Account Associated with Site -->
		<meta name="twitter:creator" content="@"/> <!-- Twitter Account Associated with Creator -->
	</head>
	
	<body>
		<?php include '../../global-header.php';?>
		<!-- PAGE CONTENT -->
		<div class="main-content-wrapper">
			<div class="therapist-bio">
				<div class="bio-info">
					<header>
						<div class="headshot">
							<img src="headshot-johncage.jpg" title="John Cage">
						</div>
						<h3>John Cage</h3>
						<h4>Trained Music Therapist and Cassical Composer</h4>
						<p>Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
					</header>
					<h5>Contact</h5>
					<p><span>Address</span> 123 Brickelberry Ln.<br/>
					<span class="bio-adress-line-2"></span>Watertown, NM 09096</p>
					<p><span>Email</span> JohnCage@johncagerocks.biz</p>
					<p><span>Phone</span> 1 + (234) 456 - 7890</p>
					<h5>Details</h5>
					<p>John Cage serves patients within a <strong>30 mile</strong> radius.</p>
					<p>John Cage <strong>does</strong> offer private sessions.</p>
				</div>
				<iframe class="bio-map" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAUl_fk5GscSUEguTS7V9RWh9309pG2wtI&q=Boston,+MA/" allowfullscreen></iframe>
			</div>
		</div>
		<!-- PAGE FOOTER -->
		<footer>
			<p>Copyright 2017 Point Motion</a>
		</footer>	
	</body>
</html>