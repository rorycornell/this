<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- TITLE -->
		<title>Point Motion | Find A Music Therapist | Edit Profile</title>

		<!-- META TAGS -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="Find a music therapist in your area.">
		<meta name="keywords" content="Find, Music, Therapist, Point, Motion, Therapy, Search, Near, Me">
	    <meta name="author" content="Point Motion">
		<meta name="copyright" content="Point Motion 2017"/>

		<!-- SCRIPTS FOR LOAD -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	    <!-- ANALYTICS -->

	    <!-- FONTS -->
	    <link href="" rel="stylesheet">

		<!-- STYLES -->
		<link rel="stylesheet" href="../../styles.css" type="text/css">
		
		<!-- FAVICONS -->
		<link rel="apple-touch-icon" sizes="180x180" href="../../ico/">
		<link rel="icon" type="image/png" sizes="32x32" href="../../ico/">
		<link rel="icon" type="image/png" sizes="194x194" href="../../ico/">
		<link rel="icon" type="image/png" sizes="192x192" href="../../ico/">
		<link rel="icon" type="image/png" sizes="16x16" href="../../ico/">
		<link rel="manifest" href="../../ico/">
		<link rel="mask-icon" href="../../ico/" color="#000000">
		<link rel="shortcut icon" href="../../ico/">
		<meta name="apple-mobile-web-app-title" content="../../ico/">
		<meta name="application-name" content="../../ico/">
		<meta name="msapplication-TileColor" content="../../ico/">
		<meta name="msapplication-TileImage" content="../../ico/">
		<meta name="msapplication-config" content="../../ico/">
		<meta name="theme-color" content="#000000">

		<!-- OPEN GRAPH -->
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://www.findamusictherapist.com"/>
		<meta property="og:description" content=""/>
		<meta property="og:title" content="Point Motion | Find A Music Therapist | Edit Profile"/>
		<meta property="og:image" content="ico/"/>

		<!-- TWITTER -->
		<meta name="twitter:card" content=""/> <!-- Card Types: “summary”, “summary_large_image”, “app”, or “player” -->
		<meta name="twitter:site" content="@"/> <!-- Twitter Account Associated with Site -->
		<meta name="twitter:creator" content="@"/> <!-- Twitter Account Associated with Creator -->
	</head>
	
	<body>
		<?php include '../../global-header.php';?>
		<!-- PAGE CONTENT -->
		<div class="main-content-wrapper">
			<div class="therapist-bio">
				<div class="bio-info">
					<header>
						<a href="javascript:;" class="headshot headshot-edit">
							<img src="headshot-johncage.jpg" title="John Cage">
						</a>
						<h3>John Cage</h3>
						<h4>Trained Music Therapist and Cassical Composer</h4>
						<label>Bio</label>
						<textarea rows="6" placeholder="">Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</textarea>
					</header>
					<h5>Contact</h5>
					<label>Address</label><input type="text" placeholder="123 Brickelberry Ln." class="form-control" name="street">
					<label>City</label><input type="text" placeholder="Watertown" class="form-control" name="city">
					<label>State</label><input type="text" placeholder="NM" class="form-control" name="state">
					<label class="no-border-bottom">Zip</label><input required="" type="text" placeholder="09096" class="form-control" name="zip">
					<p><span>Email</span> JohnCage@johncagerocks.biz</p>
					<label>Phone Number</label><input type="text" placeholder="1 + (234) 456 - 7890" class="form-control" name="phone">
					<h5>Details</h5>
					<label>Distance Served (in Miles)</label><input type="text" placeholder="30" class="form-control" name="distance_served">
					<label>Private Sessions</label><input type="checkbox" data-toggle="toggle" data-on="1" data-off="0" name="private_sessions" checked>
				</div>
				<div class="bio-map">
					<a class="profile-edit-finish" href="">Save Changes</a>
					<a class="profile-edit-cancel" href="">Cancel</a>
				</div>
			</div>
		</div>
		<!-- PAGE FOOTER -->
		<footer>
			<p>Copyright 2017 Point Motion</a>
		</footer>	
	</body>
</html>